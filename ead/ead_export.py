#! /usr/bin/python3

"""this script should get EAD from the archivespace API, make edits, and deliver the edited EAD to the Finding Aids folder on the R:\ drive"""

import sys
import subprocess
import os
import shutil
import time
import re
from asnake.client import ASnakeClient

__author__ = "Tony Lattis"
__email__ = "alattis@wisc.edu"

rep_prefix = "repositories/2/"
#rep_prefix = "repositories/3/"
file_system_location = "/mnt/r/ArchivesAdmin/Former_M/Indexes/Finding Aids/"
ead_transformation  = "/home/ajl/scripts/export_pipline/ead/prep_ead.sh"
#ead_transformation  = "sc_prep_ead.sh"

def get_resource(res_id):

    client = ASnakeClient()
    resource = client.get(rep_prefix + 'resources/' + str(res_id))
    if resource.status_code == 200:
        print("Json for resource " + resource.json()["id_0"] + " retrieved successfully.")
        return resource.json()
    else:
        print("failed to retrieve resource")

def get_ead(res_id):

    client = ASnakeClient()
    export_options = '?include_unpublished=true&include_daos=true&numbered_cs=true'
    ead = client.get(rep_prefix + "resource_descriptions/" + str(res_id) + '.xml' + export_options)
    if ead.status_code == 200:
        print("EAD for resource " + res_id + " retrieved successfully")
        return ead.text
    else:
        print("failed to retrieve resource")

def publish_all(res_id):

    client = ASnakeClient()
    client.post(rep_prefix + "resources/" + str(res_id) + "/publish")

def main():
    
    res_id = sys.argv[1]

    # check for args
    if len(sys.argv) != 2:
        print("Incorrect arguments.")
        print("Useage: python3 ead_export.py RESOURCE_ID")
        sys.exit(0)

    # get id and build file name 
    resource = get_resource(res_id)
    ead_id = resource["ead_id"]
    # extract id from ead_id string
    res = re.search(r"(uw-(ua|sc)-)(.*)", ead_id) #This regex would need to be expanded for WHS
    output_file_name = res.group(3)
    print("Processing EAD with id: " + ead_id + "...")

    # test for valid destination
    dest_file_path = file_system_location + output_file_name
    if os.path.exists(dest_file_path):
        print("File path " + dest_file_path + " valid. Processing...")
    else:
        print("File path " + dest_file_path + " invalid.")
        print("Ensure the R:\ drive is mounted and that the destination folder exists")
        sys.exit(0)

    # post to publish all values
    publish_all(res_id)
    print("Publishing all fields in " + str(ead_id))

    # get ead and write to file 
    ead = get_ead(res_id)
    output_file = open(output_file_name, "w")
    output_file.write(ead)
    output_file.close()
    print("EAD " + output_file_name + " written...")

    # run sed script of resulting file
    print("Processing " + output_file_name + "...")
    subprocess.call(["bash", ead_transformation, output_file_name])
    print("Processing for " + output_file_name + " complete.")

    # move file to finding aid folder
    final_file_name = output_file_name + ".xml"
    final_file_dest = dest_file_path + "/" + final_file_name
    shutil.copyfile(final_file_name, final_file_dest)
    print("Final EAD coppied to " + final_file_dest[38:])

    # secondary save location
    shutil.copyfile(final_file_name, "/mnt/r/ArchivesAdmin/Former_M/Indexes/Finding Aids/ead2002_new/" + final_file_name)

    # cleanup
    print("removing output file " + output_file_name + ".xml")
    os.remove(output_file_name + ".xml")
    print("removing output file " + output_file_name + ".bak")
    os.remove(output_file_name + ".bak")

if __name__ == "__main__":

    start = time.time()
    main()
    end = time.time()
    total = round(end - start, 3)
    print("Total execution time: " + str(total) + " seconds")

