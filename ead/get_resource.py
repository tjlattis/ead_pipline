import sys
import subprocess
import os
import shutil
import time
from asnake.client import ASnakeClient

__author__ = "Tony Lattis"
__email__ = "alattis@wisc.edu"

def get_resource(res_id):

    client = ASnakeClient()
    resource = client.get('repositories/3/resources/' + str(res_id))
    if resource.status_code == 200:
        print("Json for resource " + resource.json()["id_0"] + " retrieved successfully.")
        return resource.json()
    else:
        print("failed to retrieve resource")

get_resource(sys.argv[1])
