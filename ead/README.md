# ead_pipeline

> finally more stable than nordstream2!

This repository contains a python wrapper for the original ead_comply shell script. The shell script itself uses sed to make modifications to the ead exported by ArchivesSpace to make it compatible with the Archival Resources in Wisconsin resource. The script can be used on its own or in conjuection with the python wrapper ead_export.py. The wrapper implements the Asnake library to interface with the ArchivesSpace API and automatically get the ead fromArhivesSpace, run the prep_ead script against it, and move the modified ead to the desired place in the file system. 

## Installation

### script only
To run the script alone you will need the Windows Subsystem for Linux (WSL). To install the subsystem follow these [instructions](https://docs.microsoft.com/en-us/windows/wsl/install). You can use any distribution with a posix compliant shell. If you don't know what that means, install the latest Ubuntu distribution. 

Once you have a working Linux instance you can clone this repository with git:

`git clone https://gitlab.com/tjlattis/ead_pipline.git` 

Then you must make sure the script has execution privileges, for most systems this is:

`chmod +x prep_ead.sh`

### script with wrapper

The python wrapper requires some extra set up but saves a great deal of time if you are doing the process repeatedly. You will need, along with WSL, a working python3 installation and a properly installed and configured Asnake client, for which you can follow the [instructions](https://github.com/archivesspace-labs/ArchivesSnake) on the Asnake github page. Currently you must enter your ArchivesSpace password in plain text in the ArchivesSnake yml file, so it is probably good practice to change your ArchivesSpace password to something used only for that purpose. 

## Usage (script alone)

### Short version:

To use prep_ead simply invoke the script with your ead file as the first argument:

`./prep_ead uac00_0000000_00000_UTC__ead.xml`

The script will create a backup of the input file (file_name.bak) and a new file named with the Archival Resources in Wisconsin style.

This output file can be directly uploaded to Archival Resources in Wisconsin via SecureFX, however it is a good idea to open up the file in the oxygen xml editor and check for errors against the ead2002 DTD file. 

### Detailed instructions:

The script currently works by editing a file in place in the same directory as the script. To use it, first create a copy of the xml file exported from ArchivesSpace. Any method will do, but to reliably do this from the command line you can issue a `cp` command. Files in the Windows file system can be accessed from the /mnt/c directory in the linux command line. If your current working directory is the location of the script then you can issue:

`cp /mnt/c/Users/YOUR_USER_NAME/Documents/NAME_OF_EXPORTED_FILE.xml .`  

In the above command `.` will be expanded by the shell to your current directory's name. To check that all has gone well you can issue the `ls` command. If you see both the script (`prep_ead.sh`) and your input file in the list of files returned you are ready to run. 

NOTE: Do not change the name of the file as exported from ArchivesSpace. The script uses the original name of the file to create the ead id.  

Once this is done you can now run the script as above:

`./prep_ead.sh uac00_0000000_00000_UTC__ead.xml`

If it runs successfully the script will display two messages, one with the name of the backup file and another with the name of the output file. You can then copy  the output file back to the Windows file system with `cp`. In this case you want to provide the location on the Windows side as the second argument:

`cp uac00.xml /mnt/c/Users/YOUR_USER_NAME/Documents/`

You can then delete the backup file if desired. To finish the upload process simply move the file to the UWDCC servers as normal. If desired you can open the output file in Oxygen to spot check accuracy. 

NB: WSL will automatically mount the local C:\ drive at `/mnt/c` this means that you can copy files from anywhere on the C:\ drive to the current directory in wsl with `cp /mnt/c/Users/user_name/path/to/ead .`

## Usage (script with wrapper)

In order to run the wrapper successfully you must be connected to the Global Protect VPN (required to call the ArchivesSpace API) and have your destination drive mounted. Because the python wrapper cannot create folders on the windows side of your computer the location specified in the wrapper for your output ead must already exist. If the location doesn't not exist or the drive is not mounted the wrapper will nag you.

Once all of the elements for the python script to run have been installed, running the wrapper is simple. From the directory of the scripts issue

`python3 ead_export.py #####` where ##### is the resource number for the desired resource record.  

The wrapper can be modified to save output ead at an arbitrary number of locations. For UW-Archives this is two locations, one for a general archive of all processed ead and one location indexed for the collection number of that specific resource. Once the wrapper has run successfully it should cleanup all left over files and the only step remaining is to push your ead via SecureFX

