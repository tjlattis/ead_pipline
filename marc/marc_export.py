#! /usr/bin/python3

"""get MARC by Resource ID"""
"""DO NOT USE THIS TO MANIPULATE UNTRUSTED XML"""

import sys
import os
import shutil
import time
from lxml import etree as ET
from asnake.client import ASnakeClient

__author__ = "Tony Lattis"
__email__ = "alattis@wisc.edu"

def get_marc(res_id):
    client = ASnakeClient()
    resource = client.get('repositories/2/resources/marc21/' + str(res_id) + ".xml")
    if resource.status_code == 200:
        print("MARC for Resource with ID " + res_id + " retrieved successfuly")
        return resource.text
    else:
        print("Failed to retrieve MARC for resource")
        sys.exit(0)

def get_file_name(root):

    file_name = ""
    for elem in root[0].findall("{http://www.loc.gov/MARC21/slim}datafield[@tag='099']/{http://www.loc.gov/MARC21/slim}subfield"):
        file_name = elem.text
    file_name = file_name + ".xml"
    file_name = file_name.replace(" ", "_")
    file_name = file_name.replace("/", "_")
    return file_name

def register_all_namespaces(filename):
        namespaces = dict([node for _, node in ET.iterparse(filename, events=['start-ns'])])
        for ns in namespaces:
            ET.register_namespace(ns, namespaces[ns])

def configure_marc(root):

    branch = root[0]

    # remove controlfield 008
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}controlfield"):
        branch.remove(elem)

    # modify 040 $$e field 
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}datafield[@tag='040']/{http://www.loc.gov/MARC21/slim}subfield[@code='c']"):
        elem.text = "GZM"
    
    # modify 049 $$a field 
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}datafield[@tag='049']/{http://www.loc.gov/MARC21/slim}subfield[@code='a']"):
        elem.text = "GZMA"

    # add 040 $$e for rda
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}datafield[@tag='040']"):
        new_elem = ET.SubElement(elem, "{http://www.loc.gov/MARC21/slim}subfield", code='e')
        new_elem.text = "rda"

    # change 541 datafield to 561
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}datafield[@tag='541']"):
        elem.set('tag', '561')

    # remove datafield 852
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}datafield[@tag='852']"):
        branch.remove(elem)

    # remove datafield 856
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}datafield[@tag='856']"):
        branch.remove(elem)

    return root

def main():
    res_id = sys.argv[1]

    print("Fetching MARC for resource with ID: " + res_id + "...")
    marc = get_marc(res_id)
    print("Writing MARC for resource with ID: " + res_id + " to temp file...")
    output_file = open("marc21.xml", "w")
    output_file.write(marc)
    output_file.close()

    print("Parsing xml from temp file...")
    # register_all_namespaces("marc21.xml")
    tree = ET.parse("marc21.xml")
    root = tree.getroot()
    new_marc = configure_marc(root)
    file_name = get_file_name(root)

    tree.write("marc21.xml")

    # rename output file and move
    dest_file_path = "/mnt/c/Users/alattis/Documents/xml/marc/" 
    os.rename("marc21.xml", file_name)
    if os.path.exists(dest_file_path):
        print("File path " + dest_file_path + " valid. Processing...")
        dest_file_path = dest_file_path + file_name
    else:
        print("File path " + dest_file_path + " invalid.")
        print("Ensure the C:\ drive is mounted and that the destination folder exists")
        sys.exit(0)

    print("Moving new MARC file to destination folder...")
    shutil.copyfile(file_name, dest_file_path)
    if os.path.isfile(file_name):
        os.remove(file_name)
    else:
        print("Error trying to delete temp file: %s file not found" % myfile)

if __name__ == "__main__":

    start = time.time()
    main()
    end = time.time()
    total = round(end - start, 3)
    print("Total execution time: " + str(total) + " seconds")

