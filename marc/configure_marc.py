"""make systematic edits to exported marc"""

from lxml import etree as ET

def configure_marc(branch):

    # remove controlfield 008
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}controlfield"):
        branch.remove(elem)

    # modify 040 $$e field 
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}datafield[@tag='040']/{http://www.loc.gov/MARC21/slim}subfield[@code='c']"):
        elem.text = "GZM"
    
    # modify 049 $$a field 
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}datafield[@tag='049']/{http://www.loc.gov/MARC21/slim}subfield[@code='a']"):
        elem.text = "GZMA"

    # add 040 $$e for rda
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}datafield[@tag='040']"):
        new_elem = ET.SubElement(elem, "{http://www.loc.gov/MARC21/slim}subfield", code='e')
        new_elem.text = "rda"

    # change 541 datafield to 561
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}datafield[@tag='541']"):
        elem.set('tag', '561')

    # remove datafield 852
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}datafield[@tag='852']"):
        branch.remove(elem)

    # remove datafield 856
    for elem in branch.findall("{http://www.loc.gov/MARC21/slim}datafield[@tag='856']"):
        branch.remove(elem)

    tree.write('output.xml')

if __name__ == "__main__":
    
    tree = ET.parse('input_marc.xml')
    root = tree.getroot()
    configure_marc(root[0])
